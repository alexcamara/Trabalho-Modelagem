package Modelo;

import java.util.ArrayList;
import java.util.List;

import Modelo.Compra;

public class Cliente {
	
	private String nome;
	private String cpf;
	private String dataCadastro;
	private String telefone;
	private Endereco endereco;
	
private List<Compra> compras = new ArrayList<Compra>();
	
	public void adicionarCompra(Compra compra){
		compras.add(compra);
	}
	
	public void removerCompraPorNumero(int numero){
		for (Compra compra:compras){
			if (compra.getNumero() == numero ){
				compras.remove(numero);
			}
		}
	}	
	
	
	public Cliente(String nome, String cpf, String dataCadastro, String fone) {
		this.nome = nome;
		this.cpf = cpf;
		this.dataCadastro = dataCadastro;
		this.telefone = fone;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(String dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	
	public Compra pegarCompraPorNumero(int numero){
		for (Compra compra: compras){
			if (compra.getNumero() == numero){
				return compra;
			}
		}
		return null; //indica que n�o achou compra
	}
	//getCompras
	
	public List<Compra> getCompras(){
		return compras;
	}
	

}
