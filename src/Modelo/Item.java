package Modelo;


public class Item {
	
	private int codigo;
	private float qtde;
	private float preco;
	private String descricao;
	
	
	public String getDescricao(){
		return this.descricao;
	}
	
	public Item(int codigo, String descricao, float quantidade) {
		
		this.codigo = codigo;
		this.qtde = quantidade;
		this.descricao = descricao;
		
	}
	
	
	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public float getQtde() {
		return qtde;
	}
	
	public void setQtde(float quantidade) {
		this.qtde = quantidade;
	}
	
	public float getPreco() {
		return preco;
	}
	
	public void setPreco(float preco) {
		this.preco = preco;
	}
	


}
