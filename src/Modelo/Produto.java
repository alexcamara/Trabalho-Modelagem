package Modelo;

import java.util.Date;

public class Produto {
	
	private int codigo;
	private String descricao;
	private int qtdeEstoque;
	private String dataCadastro;
	private float preco;
	
	

public Produto(){}
		

	public Produto(int codigo,String descricao, int qtde, float preco) {
		this.codigo = codigo;
		this.descricao = descricao;
		this.qtdeEstoque = qtde;
		this.preco = preco;
	}

	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public int getQtde() {
		return qtdeEstoque;
	}

	public void setQtdeEstoque(int qtde) {
		this.qtdeEstoque = qtde;
	}

	public String getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro() {
		Date data = new Date();
		this.dataCadastro = data.toString();
	}

	public int getQtdeEstoque() {
		return qtdeEstoque;
	}

	public int getCodigo() {
		return codigo;
	}


	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}



	public float getPreco() {
		return preco;
	}


	public void setPreco(float preco) {
		this.preco = preco;
	}


	public void baixarProduto(int quantBaixada){
		if (quantBaixada <= this.qtdeEstoque){
			this.qtdeEstoque = this.qtdeEstoque - quantBaixada;
		}	
	}


}
