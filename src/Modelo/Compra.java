package Modelo;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import Modelo.Item;

public class Compra {
	
	private int numero;
	private String dataCompra;
	
	private List<Item> itens = new ArrayList<Item>();

	public Compra(int numero) {
		this.numero = numero;
		Date data = new Date();
		this.dataCompra = data.toString();
	}
	
	public void addItem(Item item){
		itens.add(item);
	}
	
	public void removeItemCompraPorDescricao(String descricao){
		for (Item item: itens){
			if (item.getDescricao().equals(descricao)){
				itens.remove(item);
				break;
			}
		}
	}	
		
	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	public String getDataCompra() {
		return dataCompra;
	}
	
	public float totalizarItens(){
		float total=0;
		for (Item item:itens){
			total = total + item.getQtde()*item.getPreco();
		}
		return total;
	}
	
	//getItens
	public List<Item> getItens(){
		return itens;
	}
	

}
