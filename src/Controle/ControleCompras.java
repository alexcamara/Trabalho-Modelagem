package Controle;

import java.util.ArrayList;
import java.util.List;
import java.util.*;



import Modelo.Cliente;
import Modelo.Compra;
import Modelo.Item;
import Modelo.Produto;



public class ControleCompras {

private static ControleCompras ctr=null;
	
	/*
	 * Representa o cliente selecionado em determinado momento;
	 */
	private Cliente clienteAtual=null;
	
	/*
	 * Representa o conjunto de produtos cadastrados
	 */
	private List<Produto> cadastroProduto = new ArrayList<Produto>();
	
	/*
	 * Representa o conjunto de clientes cadastrados
	 */
	private List<Cliente> cadastroCliente = new ArrayList<Cliente>();
	
	private ControleCompras(){}
	
	public static ControleCompras getInstance(){
		if (ctr == null){
			ctr = new ControleCompras();
			return ctr;
		} else
			throw new RuntimeException("Controlador j� foi criado!!");
	}
	
	/*
	 * Caso de uso - Cadastra produto 
	 * 
	 */
	Date data = new Date();
	
	public void cadastrarProduto(int codigo,String descricao,int qtde, float preco){
		Produto prod = new Produto(codigo, descricao, qtde, preco);
		boolean achou = false;
		for (Produto produto: cadastroProduto){
			if (produto.getDescricao().equals(descricao)){
				achou=true;
				break;
			}
		}
		if (! achou){
			cadastroProduto.add(prod);
		}
	}
	
	/*
	 * Caso de uso - Listar produto
	 */
	public List<String> listarProdutosCadastrados(){
		List<String> produtos = new ArrayList<String>();
		for (Produto produto: cadastroProduto){
			produtos.add(produto.getDescricao());
		}
		return produtos;
	}
	
	/*
	 * Caso de uso - Cadastrar Cliente
	 */
	public void cadastrarCliente(String nome,String cpf, String dataCadastro, String fone){
		Cliente cli = new Cliente(nome, cpf, dataCadastro, fone);
		boolean achou = false;
		for (Cliente cliente: cadastroCliente){
			if (cliente.getNome().equals(nome)){
				achou=true;
				break;
			}
		}
		if (! achou ){
			cadastroCliente.add(cli);
		}
	}
		
	
	/*
	 * Caso de uso - Listar cliente 
	 */
	public List<String> listarClientesCadastrados(){
		List<String> clientes = new ArrayList<String>();
		for (Cliente cliente: cadastroCliente){
			clientes.add(cliente.getNome());
		}
		return clientes;
	
	}
		
	/*
	 * Selecionar cliente para Compra
	 */
	public void selecionarClientePorNome(String nome){
		for (Cliente cli: cadastroCliente){
			if (cli.getNome().equals(nome)){
				clienteAtual=cli;
			}
		}
	}
	
	/*
	 * Caso de uso vendar p
	 */
	public void abrirVenda(int numero){
		Compra compra = new Compra(numero);
		if (clienteAtual != null){
			clienteAtual.adicionarCompra(compra);
		} else
		   throw new RuntimeException("Cliente n�o selecionado!!");
	}
	
	/*
	 * Caso de Uso vender produto
	 */
	public void venderProduto(int numeroCompra,String descricaoProduto,int quantidade){
		if (clienteAtual != null){
			Compra compraAtual = clienteAtual.pegarCompraPorNumero(numeroCompra);
			if (compraAtual != null){
				for (Produto produto: cadastroProduto) {
					if (produto.getDescricao().equals(descricaoProduto)) {
						compraAtual.addItem(new Item(produto.getCodigo(), descricaoProduto, quantidade));			
						produto.baixarProduto(quantidade);
					} 
				}
			} else
			 throw new RuntimeException("Compra n�o foi aberta!!");				
		} else
			throw new RuntimeException("Cliente n�o foi selecionado!!");
	}
	
	/*
	 * Totaliza as compras de um cliente 
	 * Tem que passar o número da compra
	 */
	public float totalizarVenda(int numeroCompra) {
		float total=0;
		if (clienteAtual != null) {
			Compra compraAtual = clienteAtual.pegarCompraPorNumero(numeroCompra);
			if (compraAtual != null){
				total = compraAtual.totalizarItens();
			} else
			 throw new RuntimeException("Compra n�o foi aberta!!");				
		} else
			throw new RuntimeException("Cliente n�o foi selecionado!!");
		return total;
	}
	
}
